package com.wiguiart.backend.dao;

import com.wiguiart.backend.HibernateUtil;
import com.wiguiart.backend.User;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.hibernate.Session;

public class UserDAO {

    private Connection connection;

    public UserDAO() {
        ConnectionClass con = new ConnectionClass();
        try {
            connection = con.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addUser(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
    }

    public void removeUser(int userId) {
        String query = "delete from tb_user where tb_user.id_user = " + userId + " ";
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User user) {
        String query = "update tb_user set tb_user.username='" + user.getUsername()
                + "', tb_user.password='" + user.getPassword()
                + "' where tb_user.id_user = " + user.getUserId() + " ";
        System.out.println(query);
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<User> getUsers() throws SQLException {
        String query = "select * from tb_user";
        ArrayList<User> users = new ArrayList<User>();
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        while (res.next()) {
            User user = new User();
            user.setUsername(res.getString("username"));
            user.setPassword(res.getString("password"));
            user.setUserId(res.getInt("id_user"));
            users.add(user);
        }
        return users;
    }

    public User getUserByName(String name, String password) throws SQLException {
        User user = new User();
        String query = "select * from tb_user where tb_user.username = '" + name 
                + "' AND tb_user.password = '" + password + "' ";
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        if (res.next()) {
            user.setUsername(res.getString("username"));
            user.setPassword(res.getString("password"));
            user.setUserId(res.getInt("id_user"));
        }
        return user;
    }
    
    public User getUserById(int userid) throws SQLException {
        User user = new User();
        String query = "select * from tb_user where tb_user.id_user = " + userid + " ";
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        if (res.next()) {
            user.setUsername(res.getString("username"));
            user.setPassword(res.getString("password"));
        }
        return user;
    }
}
