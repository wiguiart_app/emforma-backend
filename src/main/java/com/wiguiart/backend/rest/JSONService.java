package com.wiguiart.backend.rest;

import com.wiguiart.backend.User;
import com.wiguiart.backend.dao.UserDAO;
import java.sql.SQLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/json")
public class JSONService {

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public User getTrackInJSON() {

        User user = new User();
        user.getUsername();
        user.getPassword();

        return user;

    }

    @POST
    @Path("/add")
    public Response addUser(
            @FormParam("username") String username,
            @FormParam("password") int password) {

        return Response.status(200)
                .entity("Login : " + username + ", Senha : " + password)
                .build();

    }

    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public User loginUser(
            @FormParam("username") String username,
            @FormParam("password") String password) throws SQLException {

        UserDAO dao = new UserDAO();
        User user = new User();
        user = dao.getUserByName(username, password);

        return user;

    }

}
