/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wiguiart.backend;

/**
 *
 * @author William Fernandes
 */

import com.wiguiart.backend.dao.UserDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import org.hibernate.*;

public class HibernateTeste {

    public static void main(String[] args) throws SQLException {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        User user = new User();
        UserDAO dao = new UserDAO();
        
        user.setUsername("guilherme");
        user.setPassword("321");
        
//        user = dao.getUserByName("guilherme", "321");
        
        System.out.println("usuario " + user.getUsername());
        System.out.println("senha " + user.getPassword());
        
        

        session.save(user);

        session.getTransaction().commit();

    }
}
